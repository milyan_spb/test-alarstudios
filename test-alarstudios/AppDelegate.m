//
//  AppDelegate.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "AppDelegate.h"

//ViewControllers
#import "LoginViewController.h"

//Services
#import "Context.h"
#import "AccountService.h"
#import "NetworkService.h"
#import "APIService.h"
#import "NetworkService.h"

NSString *const kBaseURLString = @"http://condor.alarstudios.com/test";

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [mainStoryboard instantiateInitialViewController];
    
    LoginViewController *loginViewController = (LoginViewController *)navigationController.topViewController;
    
    loginViewController.context = [self context];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (Context *)context {
    Context *context = [Context new];
    
    APIService *apiService = [[APIService alloc] initWithBaseURLString:kBaseURLString];
    
    AccountService *accountService = [[AccountService alloc] init];
    accountService.apiService = apiService;
    
    context.apiService = apiService;
    context.accountSecrive = accountService;
    
    NetworkService *networkService = [[NetworkService alloc] init];
    networkService.apiService = apiService;
    networkService.accountService = accountService;
    
    context.networkService = networkService;
    
    return context;
}

@end
