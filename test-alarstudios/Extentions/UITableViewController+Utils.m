//
//  UITableViewController+Utils.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "UITableViewController+Utils.h"

@implementation UITableViewController (Utils)

- (void)registerNibWithClass:(Class)class {
    UINib *nib = [UINib nibWithNibName:NSStringFromClass(class) bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:NSStringFromClass(class)];
}

@end
