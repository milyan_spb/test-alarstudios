//
//  UIColor+Schemes.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Schemes)

+ (UIColor *)bg_button_color;
+ (UIColor *)bg_button_inactive_color;

@end
