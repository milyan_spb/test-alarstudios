//
//  UIColor+Schemes.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "UIColor+Schemes.h"

@implementation UIColor (Schemes)

+ (UIColor *)bg_button_color {
    return [UIColor colorWithRed:26 / 255.0 green:152 / 255.0 blue:210 / 255.0 alpha:1.0];
}

+ (UIColor *)bg_button_inactive_color {
    return [UIColor colorWithRed:170 / 255.0 green:170 / 255.0 blue:170 / 255.0 alpha:1.0];
}

@end
