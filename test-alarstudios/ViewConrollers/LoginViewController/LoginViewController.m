//
//  LoginViewController.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "LoginViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "DataViewController.h"

#import "LoginViewModel.h"

#import "UIColor+Schemes.h"

NSUInteger const defaultLoginButtonHeight = 36;
NSUInteger const errorLoginButtonHeight = 50;

@interface LoginViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *usernameTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLoginButtonConstraint;

@property (nonatomic, strong) LoginViewModel *viewModel;

@end

@implementation LoginViewController
@synthesize context;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewModel = [[LoginViewModel alloc] initWithContext:context];
    
    [self configureNavigationBar];
    [self configureBinding];
    [self configureUI];
}

#pragma mark - Configurations

- (void)configureNavigationBar {
    self.navigationController.navigationBarHidden = YES;
}

- (void)configureBinding {
    [[RACSignal combineLatest:@[RACObserve(self, usernameTextField.text.length), RACObserve(self, passwordTextField.text.length)] reduce:^id (NSNumber *usernameLenght, NSNumber *passwordLenght) {
        return @(usernameLenght.integerValue && passwordLenght.integerValue);
    }] subscribeNext:^(id x) {
        self.loginButton.enabled = [x boolValue];
        self.loginButton.backgroundColor = [x boolValue] ? [UIColor bg_button_color] : [UIColor bg_button_inactive_color];
    }];
}

- (void)configureUI {
    self.loginButton.layer.cornerRadius = 4;
}

#pragma mark - IBActions

- (IBAction)loginAction:(id)sender {
    [[self.viewModel loginWithUsername:self.usernameTextField.text password:self.passwordTextField.text] subscribeNext:^(id x) {
        if ([x isEqualToString:kStatusOk]) {
            [self navigateToDataViewController];
        }
        else if ([x isEqualToString:kStatusError]) {
            [self showError];
        }
    } error:^(NSError *error) {
        NSLog(@"Error");
        NSLog(@"%@", error.description);
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else if (textField == self.passwordTextField) {
        [self.passwordTextField resignFirstResponder];
    }
    return YES;
}

#pragma mark - Navigation

- (void)navigateToDataViewController {
    DataViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DataViewController class])];
    controller.context = self.context;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Animations

- (void)showError {
    [UIView animateWithDuration:1.5 delay:0.0 usingSpringWithDamping:0.2 initialSpringVelocity:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.heightLoginButtonConstraint.constant = errorLoginButtonHeight;
        [self.loginButton layoutIfNeeded];
    } completion:nil];
    [UIView animateWithDuration:0.33 delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [self.loginButton setTitle:@"Error" forState:UIControlStateNormal];
        self.loginButton.backgroundColor = [UIColor redColor];
    } completion:nil];
    [UIView animateWithDuration:0.1 delay:2.5 usingSpringWithDamping:0.3 initialSpringVelocity:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.heightLoginButtonConstraint.constant = defaultLoginButtonHeight;
        [self.loginButton layoutIfNeeded];
        self.loginButton.backgroundColor = [UIColor bg_button_color];

    } completion:^(BOOL finished) {
        [self.loginButton setTitle:@"Log in" forState:UIControlStateNormal];
    }];
}

@end
