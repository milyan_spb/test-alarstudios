//
//  LoginViewModel.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "LoginViewModel.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "Context.h"
#import "AccountService.h"

@interface LoginViewModel ()

@property (nonatomic, strong) Context *context;

@end

@implementation LoginViewModel

- (instancetype)initWithContext:(Context *)context {
    if (self = [super init]) {
        _context = context;
    }
    return self;
}

- (RACSignal *)loginWithUsername:(NSString *)username password:(NSString *)password {
    return [self.context.accountSecrive loginWithUsername:username password:password];
}

@end
