//
//  DetailDataViewController.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "DetailDataViewController.h"

#import <MapKit/MapKit.h>

#import "DataInfo.h"

@interface DetailDataViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation DetailDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MKCoordinateRegion region = [self coordinateRegionWithDataInfo:self.dataInfo];
    [self.mapView setRegion:region animated:YES];
    self.mapView.zoomEnabled = NO;
    self.mapView.scrollEnabled = NO;
    self.nameLabel.text = self.dataInfo.name;
    self.countryLabel.text = self.dataInfo.country;
}

- (MKCoordinateRegion)coordinateRegionWithDataInfo:(DataInfo *)dataInfo {
    CLLocationCoordinate2D coordinare = CLLocationCoordinate2DMake(self.dataInfo.lat, self.dataInfo.lon);
    float spanX = 0.1;
    float spanY = 0.1;
    MKCoordinateRegion region;
    region.center.latitude = coordinare.latitude;
    region.center.longitude = coordinare.longitude;
    region.span.latitudeDelta = spanX;
    region.span.longitudeDelta = spanY;
    return region;
}

@end
