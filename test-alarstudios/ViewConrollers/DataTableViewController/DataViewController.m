//
//  DataTableViewController.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "DataViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

//ViewModels
#import "DataViewModel.h"
#import "DataInfoViewModel.h"

//Views
#import "DataInfoCell.h"

//Extentions
#import "UITableViewController+Utils.h"

//Protocols
#import "ViewConfigureProtocol.h"

//ViewControllers
#import "DetailDataViewController.h"

@interface DataViewController ()

@property (nonatomic, strong) DataViewModel *viewModel;

@end

@implementation DataViewController
@synthesize context;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureNavigationBar];
    [self configureTableView];
    self.viewModel = [[DataViewModel alloc] initWithContext:self.context];
    
    [self loadNextPage];
}

#pragma mark - Configurations

- (void)configureNavigationBar {
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Data";
}

- (void)configureTableView {
    [self registerNibWithClass:[DataInfoCell class]];
}

#pragma mark - Network Requests

- (void)loadNextPage {
    if (!self.viewModel.isDownloading && !self.viewModel.isLastPage) {
        [[self.viewModel loadDataNextPage] subscribeCompleted:^{
            [self.tableView reloadData];
        }];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell <ViewConfigureProtocol> *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DataInfoCell class]) forIndexPath:indexPath];
    
    id dataInfoViewModel = [self.viewModel itemWithIndex:indexPath.row];
    [cell configureViewWithItem:dataInfoViewModel];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.viewModel.items.count - 1 == indexPath.row) {
        [self loadNextPage];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DataInfoViewModel *dataInfoViewModel = [self.viewModel itemWithIndex:indexPath.row];
    id dataInfo = dataInfoViewModel.model;
    [self navigateToDetailDataViewControllerWithDataInfo:dataInfo];
}

#pragma mark - Navigation

- (void)navigateToDetailDataViewControllerWithDataInfo:(id)dataInfo {
    DetailDataViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DetailDataViewController class])];
    controller.dataInfo = dataInfo;
    [self.navigationController pushViewController:controller animated:YES];
}

@end
