//
//  DataViewModel.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "DataViewModel.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

//Services
#import "Context.h"
#import "NetworkService.h"

//Models
#import "DataInfo.h"

//ViewModels
#import "DataInfoViewModel.h"

@interface DataViewModel ()

@property (nonatomic, strong) Context *context;

@end

@implementation DataViewModel

- (instancetype)initWithContext:(Context *)context {
    if (self = [super init]) {
        _context = context;
        _items = [@[] mutableCopy];
        _page = 1;
    }
    return self;
}

- (RACSignal *)loadDataNextPage {
    _isDownloading = YES;
    __weak DataViewModel *weakSelf = self;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[weakSelf.context.networkService loadDataWithPage:weakSelf.page] subscribeNext:^(id x) {
            _isDownloading = NO;
            _page++;
            if ([x isKindOfClass:[NSArray class]]) {
                NSArray *newItems = (NSArray *)x;
                for (DataInfo *dataInfo in newItems) {
                    [_items addObject:[[DataInfoViewModel alloc] initWithModel:dataInfo]];
                }
                _isLastPage = newItems.count != 10;
            }
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            _isDownloading = NO;
            NSLog(@"Error");
            NSLog(@"%@", error.description);
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
}

- (id)itemWithIndex:(NSUInteger)index {
    if (self.items.count > index) {
        return self.items[index];
    }
    return nil;
}

@end
