//
//  DataInfoCell.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "DataInfoCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

#import "DataInfoViewModel.h"

@interface DataInfoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation DataInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureViewWithItem:(id)item {
    if ([item isKindOfClass:[DataInfoViewModel class]]) {
        DataInfoViewModel *dataInfoViewModel = (DataInfoViewModel *)item;
        self.titleLabel.text = dataInfoViewModel.title;
        [self.picImageView sd_setImageWithURL:[NSURL URLWithString:dataInfoViewModel.picURLString]];
    }
}

@end
