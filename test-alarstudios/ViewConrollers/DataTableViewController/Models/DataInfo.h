//
//  Data.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataInfo : NSObject

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *country;
@property (nonatomic) double lat;
@property (nonatomic) double lon;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
