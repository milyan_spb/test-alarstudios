//
//  Data.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "DataInfo.h"

@implementation DataInfo

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        _identifier = dict[@"id"];
        _name = dict[@"name"];
        _country = dict[@"country"];
        _lat = [dict[@"lat"] doubleValue];
        _lon = [dict[@"lon"] doubleValue];
    }
    return self;
}

@end
