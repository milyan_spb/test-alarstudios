//
//  DataViewModel.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ViewModelProtocol.h"

@class RACSignal;
@interface DataViewModel : NSObject <ViewModelProtocol>

@property (nonatomic, strong, readonly) NSMutableArray *items;

@property (nonatomic, readonly) BOOL isDownloading;
@property (nonatomic, readonly) BOOL isLastPage;
@property (nonatomic, readonly) NSUInteger page;

- (id)itemWithIndex:(NSUInteger)index;

- (RACSignal *)loadDataNextPage;

@end
