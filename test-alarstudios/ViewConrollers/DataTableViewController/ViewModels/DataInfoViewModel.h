//
//  DataInfoViewModel.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DataInfo, UIImage;
@interface DataInfoViewModel : NSObject

@property (nonatomic, strong, readonly) id model;

@property (nonatomic, copy) NSString *picURLString;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UIImage *picImage;

- (instancetype)initWithModel:(DataInfo *)model;

@end
