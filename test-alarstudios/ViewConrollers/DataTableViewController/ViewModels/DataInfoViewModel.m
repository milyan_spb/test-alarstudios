//
//  DataInfoViewModel.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "DataInfoViewModel.h"

#import "DataInfo.h"

@implementation DataInfoViewModel

- (instancetype)initWithModel:(DataInfo *)model {
    if (self = [super init]) {
        _title = model.name;
        _model = model;
        _picURLString = @"http://v.img.com.ua/b/1100x999999/a/09/58b722ccc08f77085aa7341334c3409a.jpg";
    }
    return self;
}

@end
