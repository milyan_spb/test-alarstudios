//
//  Constants.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "Constants.h"

NSString *const kStatusOk = @"ok";
NSString *const kStatusError = @"error";
