//
//  APIService.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "APIService.h"

@interface APIService ()

@property (nonatomic, strong) NSString *baseURLString;

@end

@implementation APIService

- (instancetype)initWithBaseURLString:(NSString *)URLString {
    if (self = [super init]) {
        _baseURLString = URLString;
    }
    return self;
}

- (RACSignal *)loadRequestWithURLPath:(NSString *)URLPath parameters:(NSDictionary *)parameters HTTPMethod:(HTTPMethodType)HTTPMethod {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSString *URLString = [self URLStringWithURLPath:URLPath parameters:parameters];
        NSURLSession *session = [NSURLSession sharedSession];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:URLString]];
        request.HTTPMethod = [self stringValueWithType:HTTPMethodTypeGET];
        
        NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if (!error) {
                NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [subscriber sendNext:jsonObject];
                    [subscriber sendCompleted];
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [subscriber sendError:error];
                    [subscriber sendCompleted];
                });
            }
        }];
        
        [task resume];
        
        return nil;
    }];
}

- (NSString *)stringValueWithType:(HTTPMethodType)type {
    NSDictionary *methods = @{@(HTTPMethodTypeGET)  : @"GET",
                              @(HTTPMethodTypePOST) : @"POST",
                              @(HTTPMethodTypePUT)  : @"PUT"
                              };
    return methods[@(type)];
}

- (NSString *)URLStringWithURLPath:(NSString *)URLPath parameters:(NSDictionary *)parameters {
    NSString *URLString = self.baseURLString;
    
    URLString = [URLString stringByAppendingString:URLPath];
    
    NSUInteger countOfParameters = [parameters allKeys].count;
    if (countOfParameters) {
        URLString = [URLString stringByAppendingString:@"?"];
    }
    
    NSUInteger index = 0;
    for (NSString *key in [parameters allKeys]) {
        URLString = [URLString stringByAppendingFormat:@"%@=%@", key, parameters[key]];
        index++;
        if (index < countOfParameters) {
            URLString = [URLString stringByAppendingString:@"&"];
        }
    }
    
    return URLString;
}

@end
