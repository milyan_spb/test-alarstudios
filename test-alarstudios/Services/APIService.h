//
//  APIService.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <ReactiveCocoa/ReactiveCocoa.h>

typedef NS_ENUM(NSUInteger, HTTPMethodType) {
    HTTPMethodTypeGET = 0,
    HTTPMethodTypePOST,
    HTTPMethodTypePUT
};

@interface APIService : NSObject

- (instancetype)initWithBaseURLString:(NSString *)URLString;

- (RACSignal *)loadRequestWithURLPath:(NSString *)URLPath parameters:(NSDictionary *)parameters HTTPMethod:(HTTPMethodType)HTTPMethod;

@end
