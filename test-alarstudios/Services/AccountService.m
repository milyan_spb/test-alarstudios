//
//  AccountService.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "AccountService.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "APIService.h"

NSString *const kURLPathLogin = @"/auth.cgi";

@implementation AccountService

- (RACSignal *)loginWithUsername:(NSString *)username password:(NSString *)password {
    __weak AccountService *weakSelf = self;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSDictionary *parameters = @{@"username" : username, @"password" : password};
        
        [[weakSelf.apiService loadRequestWithURLPath:kURLPathLogin parameters:parameters HTTPMethod:HTTPMethodTypeGET] subscribeNext:^(id x) {
            if ([x isKindOfClass:[NSDictionary class]]) {
                NSDictionary *json = (NSDictionary *)x;
                if ([json[@"status"] isEqualToString:kStatusOk]) {
                    _code = json[@"code"];
                }
                [subscriber sendNext:json[@"status"]];
                [subscriber sendCompleted];
            }
        } error:^(NSError *error) {
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
}

@end
