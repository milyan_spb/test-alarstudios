//
//  DataService.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class APIService, AccountService, RACSignal;
@interface NetworkService : NSObject

@property (nonatomic, weak) APIService *apiService;
@property (nonatomic, weak) AccountService *accountService;

- (RACSignal *)loadDataWithPage:(NSUInteger)page;

@end
