//
//  DataService.m
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import "NetworkService.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

//Services
#import "AccountService.h"
#import "APIService.h"

//Models
#import "DataInfo.h"

NSString *const kURLPathGetData = @"/data.cgi";

@implementation NetworkService

- (RACSignal *)loadDataWithPage:(NSUInteger)page {
    __weak NetworkService *weakSelf = self;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSDictionary *paramaters = @{@"code" : weakSelf.accountService.code, @"p" : @(page)};
        [[weakSelf.apiService loadRequestWithURLPath:kURLPathGetData parameters:paramaters HTTPMethod:HTTPMethodTypeGET] subscribeNext:^(id x) {
            if ([x isKindOfClass:[NSDictionary class]]) {
                NSDictionary *json = (NSDictionary *)x;
                if ([json[@"status"] isEqualToString:kStatusOk]) {
                    NSArray *arrayJson = json[@"data"];
                    NSMutableArray *items = [@[] mutableCopy];
                    for (NSDictionary *dict in arrayJson) {
                        [items addObject:[[DataInfo alloc] initWithDictionary:dict]];
                    }
                    [subscriber sendNext:items];
                    [subscriber sendCompleted];
                }
                else {
                    [subscriber sendNext:json[@"status"]];
                    [subscriber sendCompleted];
                }
            }
        } error:^(NSError *error) {
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

@end
