//
//  AccountService.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;
@class APIService;
@interface AccountService : NSObject

@property (nonatomic, weak) APIService *apiService;
@property (nonatomic, copy, readonly) NSString *code;

- (RACSignal *)loginWithUsername:(NSString *)username password:(NSString *)password;

@end
