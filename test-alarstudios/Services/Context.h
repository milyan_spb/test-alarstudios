//
//  Context.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class APIService;
@class AccountService;
@class NetworkService;
@interface Context : NSObject

@property (nonatomic, strong) APIService *apiService;
@property (nonatomic, strong) AccountService *accountSecrive;
@property (nonatomic, strong) NetworkService *networkService;

@end
