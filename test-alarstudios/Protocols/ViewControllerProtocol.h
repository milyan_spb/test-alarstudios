//
//  ViewControllerProtocol.h
//  test-alarstudios
//
//  Created by Iurii Gubanov on 02.12.2017.
//  Copyright © 2017 Iurii Gubanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Context;
@protocol ViewControllerProtocol <NSObject>

@property (nonatomic, strong) Context *context;

@end
